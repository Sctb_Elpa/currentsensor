/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: porttimer.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "system.h"
#include "core.h"

extern          BOOL( *pxMBPortCBTimerExpired ) ( void );

/* ----------------------- static functions ---------------------------------*/
// using timer 2 (8)

#define MODULE_CLOCK            (FCY)

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimersInit( USHORT usTimeOut50us )
{
    uint8_t postscaler;
    uint8_t prescaler;
    int16_t devider = MODULE_CLOCK  / 1000000 * 50 * usTimeOut50us; // 17500 ok

    // autoselect prescaler and postscaler
	genPrescalerPostscalerPlank(devider,
            &prescaler, &postscaler, (unsigned char*)&PR2);

    T2CON = 0;

    T2CONbits.T2OUTPS = postscaler;
    T2CONbits.T2CKPS = prescaler;

    IPR1bits.TMR2IP = 0; // low prio
    return TRUE;
}
 


void vMBPortTimersEnable( void )
{
    TMR2 = 0;

    PIR1bits.TMR2IF = 0;
    PIE1bits.TMR2IE = 1;
    T2CONbits.TMR2ON = 1;
}


void
vMBPortTimersDisable( void )
{
    /* Disable any pending timers. */
    T2CONbits.TMR2ON = 0;
    PIE1bits.TMR2IE = 0;
}

void vMBPortTimersDelay( USHORT usTimeOutMS )
{
    // needs only for ascii mode, not leave dumy
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
void vMBTIMERExpiredISR( void )
{
    ( void )pxMBPortCBTimerExpired(  );
}

