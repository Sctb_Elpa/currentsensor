/*
 * FreeModbus Libary: PIC Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: port.h,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */


#ifndef _PORT_H
#define _PORT_H

/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <assert.h>

#define	INLINE                inline
#define PR_BEGIN_EXTERN_C     extern "C" {
#define	PR_END_EXTERN_C       }

#ifndef _XTAL_FREQ
#define _XTAL_FREQ   F_CPU
#endif

#define DIRECTION_485_PORT              LATA
#define TRANSMIT_485_BIT                0
#define DIRECTION_485_TRIS              TRISA

#define FAST_TIMER_ISR                  0
#define DISABLE_CRITICAL                1

#define set_bit(REG, n)                 REG |= (1 << n)
#define clear_bit(REG, n)               REG &= ~(1 << n)
#define toggle_bit(REG, n)              REG ^= (1 << n)

#define ENTER_CRITICAL_SECTION( )       EnterCriticalSection()
#define EXIT_CRITICAL_SECTION( )        ExitCriticalSection()

#define TRANSMIT_ON_485( )  set_bit(DIRECTION_485_PORT, TRANSMIT_485_BIT)
#define RECEIVE_ON_485( )   clear_bit(DIRECTION_485_PORT, TRANSMIT_485_BIT)

void EnterCriticalSection( void );
void ExitCriticalSection( void );

typedef char BOOL;

typedef unsigned char UCHAR;
typedef char CHAR;

typedef unsigned short USHORT;
typedef short SHORT;

typedef unsigned long ULONG;
typedef long LONG;

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

extern uint32_t speed;

#endif
