#ifndef _INA219_H_
#define _INA219_H_

#include <stdint.h>
#include <stdbool.h>

#define INA219_DEBUG 0

// INA219 memory registers
#define CONFIG_R		0x00	// configuration register
#define V_SHUNT_R		0x01	// differential shunt voltage
#define V_BUS_R			0x02	// bus voltage (wrt to system/chip GND)
#define P_BUS_R			0x03	// system power draw (= V_BUS * I_SHUNT)
#define I_SHUNT_R		0x04	// shunt current
#define CAL_R			0x05	// calibration register

#define INA_RESET		0xFFFF	// send to CONFIG_R to reset unit

#define CONFIG_DEFAULT	0x399F

// config. register bit labels
#define RST     15
#define BRNG	13
#define PG1     12
#define PG0     11
#define BADC4	10
#define BADC3	9
#define BADC2	8
#define BADC1	7
#define SADC4	6
#define SADC3	5
#define SADC2	4
#define SADC1	3
#define MODE3	2
#define MODE2	1
#define MODE1	0

#define D_GAIN              3

#define OVF     (1 << 0)

enum enINA219_Range {
    enINA219_Range_16V = 0,
    enINA219_Range_32V = 1
};

enum enINA219_PGA_Gain {
    enINA219_PGA_Gain1 = 0,
    enINA219_PGA_Gain2 = 1,
    enINA219_PGA_Gain4 = 2,
    enINA219_PGA_Gain8 = 3
};

enum enINA219_Mode {
    enINA219_Mode_PowerDown = 0,
    enINA219_Mode_ShuntVoltageTriggered = 1,
    enINA219_Mode_BusVoltageTriggered = 2,
    enINA219_Mode_ShuntAndBusTriggered = enINA219_Mode_ShuntVoltageTriggered | enINA219_Mode_BusVoltageTriggered,
    enINA219_Mode_ADCoff = 4,
    enINA219_Mode_ShuntVoltageContinues = 5,
    enINA219_Mode_BusVoltageContinues = 6,
    enINA219_Mode_ShuntAndBusContinues = enINA219_Mode_ShuntVoltageContinues | enINA219_Mode_BusVoltageContinues,
};

enum enINA219_SamplingMode {
    enINA219_SamplingMode_9bit_once = 0,
    enINA219_SamplingMode_10bit_once = 1,
    enINA219_SamplingMode_11bit_once = 2,
    enINA219_SamplingMode_12bit_once = 8,
    enINA219_SamplingMode_filter2 = 9,
    enINA219_SamplingMode_filter4 = 10,
    enINA219_SamplingMode_filter8 = 11,
    enINA219_SamplingMode_filter16 = 12,
    enINA219_SamplingMode_filter32 = 13,
    enINA219_SamplingMode_filter64 = 14,
    enINA219_SamplingMode_filter128 = 15,
};

struct INA219 {
    uint8_t i2c_address;
	float r_shunt, current_lsb, power_lsb;
	uint16_t config, cal, gain;
};
	
void INA219_new(struct INA219* this, uint8_t i2c_addr);
void INA219_configure(struct INA219* this,
    enum enINA219_Range range, enum enINA219_PGA_Gain gain,
        uint8_t bus_adc, uint8_t shunt_adc, 
        enum enINA219_Mode mode);
void INA219_reset(struct INA219* this);
void INA219_sleep(struct INA219* this);
void INA219_calibrate(struct INA219* this, float shunt_val, float v_shunt_max,
        float v_bus_max, float i_max_expected);

int16_t INA219_shuntVoltageRaw(struct INA219* this);
int16_t INA219_busVoltageRaw(struct INA219* this);

bool INA219_shuntVoltage(struct INA219* this, float *pres);
bool INA219_busVoltage(struct INA219* this, float *pres);
bool INA219_shuntCurrent_mA(struct INA219* this, float *pres);
bool INA219_busPower_mW(struct INA219* this, float* pres);

#endif /* _INA219_H_ */