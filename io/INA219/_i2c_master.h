#ifndef _I2C_MASTER_H
#define	_I2C_MASTER_H

#include <stdint.h>

void i2c_init(uint32_t rate);
void i2c_write16(uint8_t addr, uint8_t reg, uint16_t data);
int16_t i2c_read16(uint8_t addr, uint8_t reg);

#endif	/* _I2C_MASTER_H */

