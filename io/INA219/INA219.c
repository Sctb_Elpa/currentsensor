#include <delays.h>
#include <assert.h>
#include <stdbool.h>

#include "core.h"
#include "_i2c_master.h"
#include "INA219.h"

void INA219_new(struct INA219* this, uint8_t i2c_addr) {
    assert(this);
    i2c_init(100000);
    this->i2c_address = i2c_addr;
    this->gain = D_GAIN;
}

// config values (range, gain, bus adc, shunt adc, mode) can be derived from pp26-27 in the datasheet
// defaults are:
// range = 1 (0-32V bus voltage range)
// gain = 3 (1/8 gain - 320mV range)
// bus adc = 3 (12-bit, single sample, 532uS conversion time)
// shunt adc = 3 (12-bit, single sample, 532uS conversion time)
// mode = 7 (continuous conversion)
void INA219_configure(struct INA219* this,
        enum enINA219_Range range, enum enINA219_PGA_Gain gain,
        uint8_t bus_adc, uint8_t shunt_adc,
        enum enINA219_Mode mode) {
    this->config = 0;

    this->config |=
            ((uint16_t) range << BRNG) |
            ((uint16_t) gain << PG0) |
            ((uint16_t) bus_adc << BADC1) |
            ((uint16_t) shunt_adc << SADC1) |
            (uint16_t) mode;

    i2c_write16(this->i2c_address, CONFIG_R, this->config);
}

// resets the INA219
void INA219_reset(struct INA219* this) {
    i2c_write16(this->i2c_address, CONFIG_R, INA_RESET);
    Delay1KTCYx(5);
}

// sleep
void INA219_sleep(struct INA219* this) {
    INA219_configure(this, enINA219_Range_32V, enINA219_PGA_Gain8, 0, 0,
            enINA219_Mode_PowerDown);
}

// returns the raw binary value of the shunt voltage
int16_t INA219_shuntVoltageRaw(struct INA219* this) {
    return i2c_read16(this->i2c_address, V_SHUNT_R);
}

// returns the shunt voltage in volts.
bool INA219_shuntVoltage(struct INA219* this, float *pres) {
    uint16_t raw = i2c_read16(this->i2c_address, V_SHUNT_R);
    if (raw == 0xffff) {
        *pres = NAN;
        return false;
    }
    *pres = raw / 100000.0;
    return true;
}

// returns raw bus voltage binary value
int16_t INA219_busVoltageRaw(struct INA219* this) {
    return i2c_read16(this->i2c_address, V_BUS_R);
}

// returns the bus voltage in volts
bool INA219_busVoltage(struct INA219* this, float *pres) {
    int16_t raw = i2c_read16(this->i2c_address, V_BUS_R) >> 3;
    if (raw == 0xffff) {
        *pres = NAN;
        return false;
    }
    *pres = raw * 0.004;
    return true;
}

// returns the shunt current in mA
bool INA219_shuntCurrent_mA(struct INA219* this, float *pres) {
    uint16_t raw = i2c_read16(this->i2c_address, I_SHUNT_R);
    if (raw == 0xffff) {
        *pres = NAN;
        return false;
    }
    *pres = (float)raw * this->current_lsb * 1000.0;
    return true;
}

// returns the bus power in mW
bool INA219_busPower_mW(struct INA219* this, float* pres) {
    uint16_t raw = i2c_read16(this->i2c_address, P_BUS_R);
    if (raw == 0xffff) {
        *pres = NAN;
        return false;
    }
    *pres = (float)raw * this->power_lsb * 1000.0;
    return true;
}

// calibration of equations and device
// shunt_val 		= value of shunt in Ohms
// v_shunt_max 		= maximum value of voltage across shunt
// v_bus_max 		= maximum voltage of bus
// i_max_expected 	= maximum current draw of bus + shunt
// default values are for a 0.25 Ohm shunt on a 5V bus with max current of 1A
void INA219_calibrate(struct INA219* this,
        float shunt_val, float v_shunt_max, float v_bus_max, float i_max_expected) {
    uint16_t cal;
    float i_max_possible, min_lsb, max_lsb, swap;

    this->r_shunt = shunt_val;

    i_max_possible = v_shunt_max / this->r_shunt;
    min_lsb = i_max_expected / 32767;
    max_lsb = i_max_expected / 4096;

    this->current_lsb = (uint16_t)(min_lsb * 100000000) + 1;
    this->current_lsb /= 100000000;
    swap = (0.04096)/(this->current_lsb * this->r_shunt);
    cal = (uint16_t)swap;
    this->power_lsb = this->current_lsb * 20;

    i2c_write16(this->i2c_address, CAL_R, cal);
}
