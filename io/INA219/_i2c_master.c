
#include <stdint.h>
#include <i2c.h>

#include "system.h"

void i2c_init(uint32_t rate) {   
    OpenI2C(MASTER, SLEW_OFF);
    SSPADD = F_CPU / (4 * rate) - 1;
}

// writes a 16-bit word (data) to register pointer (reg)
// when selecting a register pointer to read from, (d) = 0
void i2c_write16(uint8_t addr, uint8_t reg, uint16_t data) {
    StartI2C();
    WriteI2C(addr << 1);
    WriteI2C(reg);
    WriteI2C((uint8_t)(data >> 8));
    WriteI2C((uint8_t)data);
    StopI2C();
}

int16_t i2c_read16(uint8_t addr, uint8_t reg) {
    union {
        uint16_t res;
        uint8_t b[2];
    } ret;

    StartI2C();
    WriteI2C(addr << 1);
    WriteI2C(reg);
    RestartI2C();
    WriteI2C((addr << 1) | 1);
    ret.b[1] = ReadI2C();
    AckI2C();
    ret.b[0] = ReadI2C();
    NotAckI2C();
    StopI2C();

    return ret.res;
}