/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* TODO Define system operating frequency */

/* Microcontroller MIPs (FCY) */
/* Microcontroller MIPs (FCY) */
#ifndef FCPU
#define FCPU            10000000UL
#endif

#ifndef NOT_USE_PLL
#define SYS_FREQ        (FCPU * 4)
#else
#define SYS_FREQ        (FCPU)
#endif

#define F_CPU           SYS_FREQ

#define FCY             (SYS_FREQ/4)

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */
