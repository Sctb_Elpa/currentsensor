/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>

#include "user.h"
#include "dataModel.h"
#include "mbcrc.h"
#include "shaduler.h"
#include "port.h"

#if REG_HOLDING_NREGS > 0
union unHoldings RegHoldingBuf;
union unHoldings RegHoldingBuf_tmp;
const uint8_t ucRegHoldongROMask[] = {0b00000001, 0, 0, 0, 0};
#endif

#if REG_INPUT_NREGS > 0
union unInputs RegInputBuf;
#endif

#if COILS_NCOILS > 0
union unCoils CoilsBuf;
#endif

#if DISCRET_INPUTS_NINPUTS > 0
union unDiscretInputs DiscretInputsBuf;
#endif

#ifndef set_bit
#define set_bit(REG, n)                 REG |= (1 << n)
#endif

#ifndef clear_bit
#define clear_bit(REG, n)               REG &= ~(1 << n)
#endif

eMBErrorCode    eMBClose( void );
eMBErrorCode    eMBEnable( void );
eMBErrorCode    eMBDisable( void );

void (*processAfterAnsver)(void) = NULL;

void ApplyHoldings() {
	if (holdingValidator() == MB_ENOERR)
		memcpy(RegHoldingBuf.raw, RegHoldingBuf_tmp.raw, sizeof(union unHoldings));
}

eMBErrorCode holdingValidator() {
	uint8_t tmp;
	static const uint16_t valid_speeds[] = { 2400, 9600, 19200, 38400, 57600, 0xffff };

	if (RegHoldingBuf_tmp.names.ID != DEVICE_ID)
		goto __failed;

	if (RegHoldingBuf_tmp.names.Adress == 0 || 
		RegHoldingBuf_tmp.names.Adress > 247)
		goto __failed;

	if (RegHoldingBuf_tmp.names.cyclicalSendPeriod < 20)
		goto __failed;

	for (tmp = 0; tmp < sizeof(valid_speeds) / sizeof(uint16_t); ++tmp)
		if (RegHoldingBuf_tmp.names.Speed == valid_speeds[tmp])
			goto __speed_ok;

__failed:
	memcpy(RegHoldingBuf_tmp.raw, RegHoldingBuf.raw, sizeof(union unHoldings)); // restore
	return MB_EINVAL;

__speed_ok:
	return MB_ENOERR;
}

uint8_t __div8(uint8_t val) {
    return val >> 3;
}

uint8_t __mod8(uint8_t val) {
    return val - (__div8(val) << 3);
}

static void __copy(uint8_t* to, uint8_t* from) {
    *to++ = *(++from);
    *to = *(--from);
}


static void resetDefaults() {
    struct EEPROMContainer container;
    uint8_t i;

    container.holdings.names.ID = DEVICE_ID;
    container.holdings.names.Serial = 0;
    container.holdings.names.Adress = DEVICE_ADDR_DEFAULT;
    container.holdings.names.Speed = DEVICE_SPEED_DEFAULT;
    container.holdings.names.Password = 0;
    container.holdings.names.cyclicalSendPeriod = 20;
    memset(container.Coils.raw, 0, sizeof (union unCoils));

    container.CRC = usMBCRC16((uint8_t*)&container + sizeof(uint16_t),
            sizeof(struct EEPROMContainer) - sizeof(uint16_t));

     // write defaults
    for(i = 0; i < sizeof(struct EEPROMContainer); ++i)
    {
        CLRWDT();
        EEPROM_WRITE(i, ((uint8_t*)&container)[i]);
    }

    RESET();
}

eMBErrorCode
eMBRegInputCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNRegs) {
#if (REG_INPUT_NREGS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iRegIndex = 0;

    --usAddress;

    if ((usAddress >= REG_INPUT_START) &&
            (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS)) {
		iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START);
        while (usNRegs-- > 0) {
            __copy(pucRegBuffer, (uint8_t*) & RegInputBuf.raw[iRegIndex]);
            iRegIndex++;
            pucRegBuffer += sizeof (uint16_t);
        }
    } else {
        eStatus = MB_ENOREG;
    }

    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegHoldingCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNRegs,
        eMBRegisterMode eMode) {
#if (REG_HOLDING_NREGS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iRegIndex;

    --usAddress;
    if ((usAddress >= REG_HOLDING_START) &&
            (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS)) {
        iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START); 
        if (eMode == MB_REG_READ) { 
            while (usNRegs-- > 0) {
                __copy(pucRegBuffer, (uint8_t*) & RegHoldingBuf.raw[iRegIndex]);
                iRegIndex++;
                pucRegBuffer += sizeof (uint16_t);
            }
        } else {
            uint8_t i = usNRegs;
            while (i > 0) {
                if (test_bit(ucRegHoldongROMask[__div8(iRegIndex)], __mod8(iRegIndex)))
                    return MB_EINVAL;
                ++iRegIndex;
                --i;
            }

            iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START);
            while (usNRegs-- > 0) {
                static uint8_t ii = 0;
                if (ii++)
                    ii = 0;
                __copy((uint8_t*) & RegHoldingBuf_tmp.raw[iRegIndex], pucRegBuffer);
                iRegIndex++;
                pucRegBuffer += sizeof (uint16_t);
            }
			eStatus = holdingValidator();
        }
    } else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegCoilsCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNCoils,
        eMBRegisterMode eMode) {
#if (COILS_NCOILS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iCoilIndex;
    uint16_t tmp = 0;
    --usAddress;
    if ((usAddress >= COILS_START)
            && (usAddress + usNCoils <= COILS_START + COILS_NCOILS)) {
        iCoilIndex = (uint16_t) (usAddress - COILS_START);
        if (eMode == MB_REG_READ) { // read
            memset(pucRegBuffer, 0, __div8(usNCoils) + 1);
            for (; tmp < usNCoils; ++tmp) {
                if (test_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex)))
                    set_bit(*pucRegBuffer, __mod8(tmp));
                ++iCoilIndex;
                if (!__mod8(iCoilIndex))
                    ++pucRegBuffer;
            }
        } else {// write
            for (; tmp < usNCoils; ++tmp) {
                if (test_bit(pucRegBuffer[__div8(tmp)], __mod8(tmp)))
                    set_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex));
                else
                    clear_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex));
                ++iCoilIndex;
            }
            processCoils();
        }
    } else {
        eStatus = MB_ENOREG;
    }

    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegDiscreteCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNDiscrete) {
#if (DISCRET_INPUTS_NINPUTS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iDiscreteIndex;
    --usAddress;
    //??????
    if ((usAddress >= COILS_START) &&
            (usAddress + usNDiscrete <= DISCRET_INPUTS_START + DISCRET_INPUTS_NINPUTS)) {
        uint16_t tmp = 0;
        iDiscreteIndex = (uint16_t) (usAddress - DISCRET_INPUTS_START);
        memset(pucRegBuffer, 0, __div8(usNDiscrete) + 1);
        for (; tmp < usNDiscrete; ++tmp) {
            if (test_bit(DiscretInputsBuf.raw[__div8(iDiscreteIndex)], __mod8(iDiscreteIndex)))
                set_bit(*pucRegBuffer, __mod8(tmp));
            ++iDiscreteIndex;
            if (!__mod8(iDiscreteIndex))
                ++pucRegBuffer;
        }
    } else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
#else
    return MB_ENOREG;
#endif
}

void InitDataModel() {
    uint16_t i;

    // read eeprom
    struct EEPROMContainer container;
    for(i = 0; i < sizeof(struct EEPROMContainer); ++i)
        ((uint8_t*)&container)[i] = EEPROM_READ(i);

    
    // check
	CLRWDT();
    if (usMBCRC16((uint8_t*)&container + sizeof(uint16_t),
        sizeof(struct EEPROMContainer) - sizeof(uint16_t)) != container.CRC)
    {
        resetDefaults();
    }
    memcpy(RegHoldingBuf.raw, container.holdings.raw, sizeof(union unHoldings));
	memcpy(RegHoldingBuf_tmp.raw, container.holdings.raw, sizeof(union unHoldings));
    memcpy(CoilsBuf.raw, container.Coils.raw, sizeof(union unCoils));
	CLRWDT();
    memset(RegInputBuf.raw, 0, sizeof(union unInputs));
    memset(DiscretInputsBuf.raw, 0, sizeof (union unDiscretInputs));
}

void saveSettings() {
    uint16_t i;
    struct EEPROMContainer container;

	// save before appling
	if (holdingValidator() == MB_ENOERR)
		memcpy(container.holdings.raw, RegHoldingBuf_tmp.raw, sizeof(union unHoldings));
	else
    	memcpy(container.holdings.raw, RegHoldingBuf.raw, sizeof(union unHoldings));
    memcpy(container.Coils.raw, CoilsBuf.raw, sizeof(union unCoils));

	container.Coils.names.InfinitySendMode = 0;
    container.CRC = usMBCRC16((uint8_t*)&container + sizeof(uint16_t),
            sizeof(struct EEPROMContainer) - sizeof(uint16_t));

	eMBDisable();
    for(i = 0; i < sizeof(struct EEPROMContainer); ++i)
	{
		CLRWDT();
        EEPROM_WRITE(i, ((uint8_t*)&container)[i]);
	}
	eMBEnable();
}

static void apply_save() {
	applySettings();
	shadule(saveSettings);
}


void processCoils() {
    if (CoilsBuf.names.WriteSettings)
    {
        CoilsBuf.names.WriteSettings = 0;
		if (CoilsBuf.names.ApplySettings)
			processAfterAnsver = apply_save;
		else
        	processAfterAnsver = saveSettings;
		return;
    }
    if (CoilsBuf.names.ApplySettings)
    {
        CoilsBuf.names.ApplySettings = 0;
        processAfterAnsver = applySettings;
    }
}
