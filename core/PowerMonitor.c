
#include <pic18f25k80.h>

#include "dataModel.h"
#include "INA219.h"
#include "PowerMonitor.h"
#include "core.h"

#define INA219_ADDR             0b1000000
#define SHUNT_VALUE             0.3
#define MAX_SHUNT_VOLTAGE       0.2
#define MAX_BUS_VOLTAGE         15
#define MAX_CURRENT_EXPECTED    1.0

enum states {
    STATE_READ_SHUNT_VOLTAGE = 0,
    STATE_READ_BUS_VOLTAGE,
    STATE_READ_POWER,
    STATE_READ_CURRENT,
    
    STATES_COUNT,
};

static struct INA219 PowerMonitor;


void PowerMonitor_Init() {
    INA219_new(&PowerMonitor, INA219_ADDR);
}

void PowerMonitor_process() {
    static enum states state = STATE_READ_SHUNT_VOLTAGE;
    bool res;
    
    switch (state){
        case STATE_READ_SHUNT_VOLTAGE:
            res = INA219_shuntVoltage(&PowerMonitor, &RegInputBuf.names.ShuntVoltage);
            break;
        case STATE_READ_BUS_VOLTAGE:
            res = INA219_busVoltage(&PowerMonitor, &RegInputBuf.names.Voltage);
            break;
        case STATE_READ_POWER:
            res = INA219_busPower_mW(&PowerMonitor, &RegInputBuf.names.Power);
            break;
        case STATE_READ_CURRENT:
            res = INA219_shuntCurrent_mA(&PowerMonitor, &RegInputBuf.names.Current);
            break;
    }
    
    DiscretInputsBuf.names.sensor_error = !res;

    state = (state + 1) % STATES_COUNT;
}

void PowerMonitor_enable(char enabled) {
    if (enabled) {
        INA219_reset(&PowerMonitor);
        INA219_configure(&PowerMonitor, enINA219_Range_16V, enINA219_PGA_Gain8,
                enINA219_SamplingMode_filter128, enINA219_SamplingMode_filter16,
                enINA219_Mode_ShuntAndBusContinues);
        INA219_calibrate(&PowerMonitor, SHUNT_VALUE, MAX_SHUNT_VOLTAGE, 
            MAX_BUS_VOLTAGE, MAX_CURRENT_EXPECTED);
    } else {
        INA219_sleep(&PowerMonitor);   
    }
}
