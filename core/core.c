/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>
#include <stdlib.h>

#include "mb.h"
#include "dataModel.h"
#include "system.h"
#include "shaduler.h"
#include "user.h"
#include "PowerMonitor.h"

#include "core.h"

unsigned long maxU = ~0;

void initCore() {
    InitDataModel();
    PowerMonitor_Init();
    PowerMonitor_enable(!CoilsBuf.names.PowerSaveMode);
    enableShaduler(1);
}

void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank) {
    static const uint8_t prescalers[] = {1, 4, 16};
    uint8_t postscaler;
    uint8_t i;
    uint8_t _plank = 0xff;

    for (i = 0; i < sizeof (prescalers); ++i)
        for (postscaler = 1; postscaler <= 16; ++postscaler) {
            uint16_t need_dev = Kd / (prescalers[i] * postscaler);
            if (need_dev < 256) {
                _plank = need_dev;
                goto __end_genPrescalerPostscalerStart;
            }
        }

__end_genPrescalerPostscalerStart:
    *PrescalerVal = i;
    *postscalerVal = postscaler - 1;
    *plank = _plank;
}

void ResetOutput() {
    RegInputBuf.names.ShuntVoltage = NAN;
    RegInputBuf.names.Voltage = NAN;
    RegInputBuf.names.Power = NAN;
    RegInputBuf.names.Current = NAN;
}

void update_pv() {
    if (!CoilsBuf.names.PowerSaveMode)
        PowerMonitor_process();
    else
        ResetOutput();
}