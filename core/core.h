/* 
 * File:   core.h
 * Author: tolyan
 *
 * Created on 6 ???? 2015 ?., 12:46
 */

#ifndef CORE_H
#define	CORE_H

#include <stdint.h>

#ifndef NAN
extern unsigned long maxU;
#define NAN  	(*((float*)&maxU))
#endif

void initCore();
void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank);
void ResetOutput();
void update_pv();

#endif	/* CORE_H */

