/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* High-priority service */
#include "port.h"
#include "user.h"

#include "shaduler.h"
#include "mbport.h"
#include "mb.h"

#include "dataModel.h"

#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
    RESET();
}

extern void vMBTIMERExpiredISR( void ); // modbus timer ISR

/* Low-priority interrupt routine */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
    // UART interrupts
    if(PIE3bits.RC2IE && PIR3bits.RC2IF)
    {
        PIR3bits.RC2IF = 0;
        pxMBFrameCBByteReceived();
    }
    if(PIE3bits.TX2IE && PIR3bits.TX2IF)
    {
        PIR3bits.TX2IF = 0;
        pxMBFrameCBTransmitterEmpty();
    }

    // modbus timeout interrupt
    if (PIE1bits.TMR2IE && PIR1bits.TMR2IF)
    {
        PIR1bits.TMR2IF = 0;
        vMBTIMERExpiredISR();
    }

    //shaduler
    if (PIE4bits.TMR4IE && PIR4bits.TMR4IF)
    {
        PIR4bits.TMR4IF = 0;
        tick();
    }
}
