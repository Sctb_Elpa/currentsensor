/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/


void InitApp(void);             /* I/O and Peripheral Initialization */
void UserMain();                /* user main loop body */
void applySettings();           /* apply settings what needs restart modules */
void processTimeCrytical();		/* protocols state mashins and event loop */
void initMB();
