#include "core.h"
#include "mb.h"
#include "port.h"
#include "shaduler.h"
#include "dataModel.h"
#include "PowerMonitor.h"

#include "user.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

void initMB() {
	eMBErrorCode err;
	if (RegHoldingBuf.names.Speed == 0xffff)
		err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 2,
	            115200, MB_PAR_NONE);
	else
	    err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 2,
	            RegHoldingBuf.names.Speed, MB_PAR_NONE);
    if (err == MB_EINVAL)
        // try default
        eMBInit(MB_RTU, DEVICE_ADDR_DEFAULT, 2, DEVICE_SPEED_DEFAULT,
                MB_PAR_NONE);
}

void InitApp(void) {
    initCore();

    /* Initialize modbus */
    initMB();
    
    /* Disable unnessusary modules*/
    // disable analog inputs
    ANCON0 = 0;
    ANCON1 = 0;
    PMD0 = ~((1 << 0) | (1 << 2)); // disable all but UART2 and MSSP
    PMD1 = (1 << 7) | (1 << 6) | (1 << 5); // disable PSP, CTMU, ADC
    PMD2 = 0b1111; // disable all

    /* Configure the IPEN bit (1=on) in RCON to turn on/off int priorities */
    RCONbits.IPEN = 1;
}

extern UCHAR    ucMBAddress;

void applySettings() {
	// apply variables
	ApplyHoldings();

    // restart modbus
	if ((speed == 115200 && RegHoldingBuf.names.Speed != 0xffff) ||
		speed != RegHoldingBuf.names.Speed || 
		RegHoldingBuf.names.Adress != ucMBAddress) {
	    eMBDisable();
	    eMBClose();
		initMB();
		eMBEnable();
	}

    PowerMonitor_enable(!CoilsBuf.names.PowerSaveMode);

    // restart shaduler
    enableShaduler(1);
}

void processTimeCrytical() {
	CLRWDT();
	eMBPoll();
	ProcessShaduled(); // exec shaduled task if needed
}

void UserMain() {
    processTimeCrytical();
}
